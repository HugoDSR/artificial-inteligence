#
# Authors: Luis Seabra Lopes
# October-November 2013
#

"""
TODO
change self.path[0][0] to target
clean up
refactor
test
"""

from croblink import *
from math import *

class SearchDomain:
    def __init__(self, grelha):
        self.grelha = grelha
    def actions(self, state, dist):
        return self.grelha.vizinhos(state, dist)
    # custo de uma accao num estado
    def cost(self, state, action):
        #vertical por horizontal
        if state.x == action.x or state.y == action.y:
          return 10
        
        return 14

# Problemas concretos a resolver
# dentro de um determinado dominio
class SearchProblem:
    def __init__(self, domain, initial):
        self.domain = domain
        self.initial = initial
    def goal_test(self, state):
        return state.x == 0 and state.y == 0

class SearchN:
    def __init__(self,state,parent,cost,heuristica): 
        self.state = state
        self.parent = parent
        self.cost = cost
        self.heuristica = heuristica
    def __str__(self):
        return "(" + self.state + ", " + self.parent.state + ", " + str(self.cost) \
		 + ", " + str(self.heuristica) + ")"

class SearchTree:
    # construtor
    def __init__(self,problem): 
        self.problem = problem
        root = SearchN(problem.initial, None, 0, problem.initial.heuristic)
        self.open_nodes = [root]

    # obter o caminho (sequencia de estados) da raiz ate um no
    def get_path(self, node):
        if node.parent == None:
            return [node.state]

        path = self.get_path(node.parent)
        path += [node.state]
        return(path)

    # procurar a solucao
    def search(self):
        #self.custoTotal = 0
        self.costSolucao = 0
        self.num = 0

        while self.open_nodes != []:
            node = self.open_nodes[0]

            if self.problem.goal_test(node.state):
                self.costSolucao = node.cost
                return self.get_path(node)

            self.open_nodes[0:1] = []

            #vizinhos
            vizinhos = self.problem.domain.actions(node.state, 1)
            lnewnodes = []

            b = self.get_path(node)
            """
            c = ''
            for a in b:
              c += '(' + str(a.x) + ', ' + str(a.y) + ')'
            print c
            """
            #print vizinhos
            #if node.parent != None:
            #  print 'state', node.state, node.parent.state.x, node.parent.state.y
            self.num += 1
            for v in vizinhos:
 		new = True
                #print v
                for n in self.get_path(node):
                  #print 'kkkk'
                  if n.x == v.x and n.y == v.y:
                    new = False
                    break

                if new:
                  newnode = SearchN(v, node, node.cost + self.problem.domain.cost(node.state, v), v.heuristic)
                  lnewnodes += [newnode]

            #for i in lnewnodes:
            #  print i.state, i.cost, i.heuristica
            #if self.num == 20:
            #  quit()
            self.add_to_open(lnewnodes)

    # juntar novos nos a lista de nos abertos de acordo com a estrategia
    def add_to_open(self,lnewnodes):
      self.open_nodes.extend(lnewnodes)
      self.open_nodes.sort(key = lambda n: n.heuristica + n.cost)

# Nos de uma arvore de pesquisa
class SearchNode:
    def __init__(self, x, y, gps, visitado, obstacle, initialGPS): 
        self.x = x
        self.y = y
        self.gps = gps
        self.visitado = visitado
        self.candidate = False
        self.obstacle = obstacle
        self.heuristic = self.heuristics(gps, initialGPS)
        self.end = 0
        self.path = 0

    def __str__(self):
        return '( x = ' + str(self.x) + ', y = ' + str(self.y) + ', gps = (' + str(self.gps[0]) + ', ' + str(self.gps[1]) + ')' \
		+ ', visitado = ' + str(self.visitado) + ', obstacule = ' + str(self.obstacle) + ', cand = ' + str(self.candidate) + ')'
    
    def heuristics(self, state, goal_state):
        (c1,c2) = state
        (x,y)=goal_state
        return hypot(x-c1, y-c2)
    
class Grelha2:
    def __init__(self, gps, tamanho):
        #comecamos isto por criar um quadrado de 40 por 40
        #ficamos com todos ja candidatos a ser visitados, e esta pode ser aumentada. Mesmo nao sabendo o tamanho do mapa
        #isto nao influencia porque nao exploramos por isto, apenas regressamos pelo que ja esta representado ;)
        self.tamanho = tamanho
        self.all_nodes = [SearchNode(0, 0, gps, True, False, gps)]
        #ncell = (tamanho*20)/2
        self.numCol = int(28 / self.tamanho) + 1
        self.numLin = int(14 / self.tamanho) + 1

        #print self.numCol, self.numLin

        for x in range(self.numCol):
            for y in range(self.numLin):
                if([node for node in self.all_nodes if node.x==x and node.y==y] == []):
                    self.all_nodes.append(SearchNode(x, y, (gps[0] + (self.tamanho * x), gps[1] + (self.tamanho * y)), False, False, gps))

        for x in range(self.numCol):
            for y in range(self.numLin):
                if([node for node in self.all_nodes if node.x==x and node.y==-y] == []):
                    self.all_nodes.append(SearchNode(x, -y, (gps[0] + (self.tamanho * x), gps[1] + (self.tamanho * -y)), False, False, gps))

        for x in range(self.numCol):
            for y in range(self.numLin):
                if([node for node in self.all_nodes if node.x==-x and node.y==y] == []):
                    self.all_nodes.append(SearchNode(-x, y, (gps[0] + (self.tamanho * -x), gps[1] + (self.tamanho * y)), False, False, gps))

        for x in range(self.numCol):
            for y in range(self.numLin):
                if([node for node in self.all_nodes if node.x==-x and node.y==-y] == []):
                    self.all_nodes.append(SearchNode(-x, -y, (gps[0]+(self.tamanho * -x), gps[1] + (self.tamanho * -y)), False, False, gps))
    
    def runX(self, y):
          line = ''
          x = self.numCol * -1 + 1
          while x <= 0:
            #print x, y
            cell = [no for no in self.all_nodes if no.x == x and no.y == y]
            if cell == []:
              line += ' '
            elif x == 0 and y == 0:
              line += 'i'
            elif cell[0].path == 1:
              line += 'o'
            elif cell[0].end == 1:
              line += 'F'
            elif cell[0].obstacle:
              line += '*'
            elif cell[0].visitado:
              line += '.'
            elif cell[0].candidate:
              line += '?'
            elif not cell[0].visitado:
              line += ' '

            x += 1
          
          x = 1
          while x <= self.numCol - 1:
            cell = [no for no in self.all_nodes if no.x == x and no.y == y]
            if cell == []:
              line += ' '
            elif cell[0].obstacle:
              line += '*'
            elif cell[0].path == 1:
              line += 'o'
            elif cell[0].end == 1:
              line += 'F'
            elif cell[0].visitado:
              line += '.'
            elif cell[0].candidate:
              line += '?'
            elif not cell[0].visitado:
              line += ' '
            
            x += 1

          return line

    def printMe(self):
        #for n in self.all_nodes:
        #  print n

        #refactor
        y = self.numLin - 1
        while y >= 0:
          line = self.runX(y)
          y -= 1
          print line

        y = -1
        while y >= self.numLin * -1 + 1:
          line = self.runX(y)
          y -= 1
          print line

    def getCellByXAndY(self, x, y):
        cell = [n for n in self.all_nodes if n.x == x and n.y == y]
        if cell != []:
          return cell[0]

        return None

    def checkForObstacle(self, coords1, coords2):
        print coords1

        if coords1[0] == coords2[0]:
            lista = []
            if(coords2[1]>coords1[1]):
                lista = [x for x in self.all_nodes if x.x == coords1[0] and (x.y>coords1[1]and x.y<=coords2[1]) and x.obstacle==True]
            else:
                lista = [x for x in self.all_nodes if x.x == coords1[0] and (x.y>=coords2[1]and x.y<coords1[1]) and x.obstacle==True]

            if lista==[]:
                return True
            else:
                return False

        if coords1[1] == coords2[1]:
            lista = []
            if(coords2[0]>coords1[0]):
                lista = [x for x in self.all_nodes if x.y == coords1[1] and (x.x>coords1[0]and x.x<=coords2[0]) and x.obstacle==True]
            else:
                lista = [x for x in self.all_nodes if x.y == coords1[1] and (x.x>=coords2[0]and x.x<coords1[0]) and x.obstacle==True]

            if lista==[]:
                return True
            else:
                return False
        else:
            lista =[]
            if coords1[0]>coords2[0]:
                if coords1[1]>coords2[1]:
                    lista = [x for x in self.all_nodes if (coords1[0]>=x.x>=coords2[0]) and (coords1[1]>=x.y>=coords2[1]) and x.obstacle== True]
                if coords2[1]>coords1[1]:
                    lista = [x for x in self.all_nodes if (coords1[0]>=x.x>=coords2[0]) and (coords2[1]>=x.y>=coords1[1]) and x.obstacle== True]
            if coords1[0]<coords2[0]:
                if coords1[1]>coords2[1]:
                    lista = [x for x in self.all_nodes if (coords2[0]>=x.x>=coords1[0]) and (coords1[1]>=x.y>=coords2[1]) and x.obstacle== True]
                if coords2[1]>coords1[1]:
                    lista = [x for x in self.all_nodes if (coords2[0]>=x.x>=coords1[0]) and (coords2[1]>=x.y>=coords1[1]) and x.obstacle== True]
            if lista==[]:
                return True
            else:
                return False
    """
    def vizinhos(self, snode, dist):
        dirs = [[dist, 0], [dist, dist], [0, dist], [-dist, dist], [-dist, 0],[-dist, -dist],[0, -dist],[dist, -dist]]
        result = []
        for dir in dirs:
            neighbor = [snode.x + dir[0], snode.y + dir[1]]
            #print neighbor
            if self.checkForObstacle((snode.x,snode.y),((snode.x + dir[0]), (snode.y + dir[1]))):
                result += [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1] \
			             and (n.visitado or n.candidate) and not n.obstacle] 

            #print result
            #if [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1]] !=[]:
            #    result += [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1]]
        return result
    """
    def vizinhos(self, snode, dist):
        dirs = [[dist, 0], [dist, dist], [0, dist], [-dist, dist], [-dist, 0],[-dist, -dist],[0, -dist],[dist, -dist]]
        result = []
        for dir in dirs:
            neighbor = [snode.x + dir[0], snode.y + dir[1]]
            #print neighbor
            result += [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1] \
			and (n.visitado or n.candidate) and not n.obstacle] 

            #print result
            #if [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1]] !=[]:
            #    result += [n for n in self.all_nodes if n.x == neighbor[0] and n.y == neighbor[1]]
        return result
    
    def proximity(self, p, q):
        #a usar a mesma distancia do professor quando ele para o robo por estar proximo do ponto inicial
        return sqrt((p.x-q.x)**2 + (p.y-q.y)**2)<0.5

    def getCurrent(self, gps):
        #f = lambda q,p: True if sqrt((p[0]-q[0])**2 + (p[1]-q[1])**2)<(self.tamanho/2) else False
        lista = [no for no in self.all_nodes if (sqrt((gps[0]-no.gps[0])**2 + (gps[1]-no.gps[1])**2) <= self.tamanho)]
        if lista != []:
            return lista[0]
        else:
            return None

    def bestDecision(self, gps, dist):
        no = self.getCurrent(gps)
        if(no != None):
            lista =  self.vizinhos(no, dist)
            lista.sort(key = lambda node: node.heuristic)
            if lista != []:
              return lista[0]

        return None

############################################################################
#guarda info sobre o agente
class prevAgentState:
  def __init__(self):
    self.maxX = -1
    self.maxY = -1
    self.minX = -1
    self.minY = -1
    self.mapCorner = False
    self.lastX = -1
    self.lastY = -1
    self.currX = -1
    self.currY = -1
    self.lastOrientation = ''
    self.turnL = False
    self.numTurnsL = 0
    self.turnR = False
    self.numTurnsR = 0
    self.beacon = ()
    self.seenBeacon = False
    self.beginPos = ()
  #limita o erro a 0.5
  def setCurrentPosition(self, x, y):
    self.currX = x
    self.currY = y

    if self.lastX != -1 and abs(self.lastX - self.currX) > 0.5 and abs(self.lastX - self.currX) <= 1:
      self.currX = (self.currX + self.lastX) / 2
    elif self.lastX != -1 and abs(self.lastX - self.currX) > 1:
      if self.lastX < self.currX:
        self.currX = self.lastX + 0.5
      else:
        self.currX = self.lastX - 0.5

    self.lastX = self.currX
    
    if self.lastY != -1 and abs(self.lastY - self.currY) > 0.5 and abs(self.lastY - self.currY) <= 1:
      self.currY = (self.currY + self.lastY) / 2
    elif self.lastY != -1 and abs(self.lastY - self.currY) > 1:
      if self.lastY < self.currY:
        self.currY = self.lastY + 0.5
      else:
        self.currY = self.lastY - 0.5
    
    self.lastY = self.currY
    #print self.currX, self.currY
  def chooseSide(self, orientation, x, y):
    if orientation == 'n':
      #percorri menos de dois terco do max possivel de um mapa
      if (self.maxX - self.minX) < 28 * 2/3:
        #se percorreu pouco espaco afasta-se da origem para tentar explorar mais
        if abs(self.beginPos[1] - (x + 1)) > abs(self.beginPos[1] - (x - 1)):
          return 'l'
        else:
          return 'r';
    if orientation == 'e' or orientation == 'se' or orientation == 'sw':
      #percorri menos de um terco do max possivel de um mapa
      if (self.maxY - self.minY) > 14 * 2 / 3:
        #se esta proximo do maximo em cima e nao ve o farol vai para baixo
        if abs(self.maxY - y) < 6:
          return 'l'
        else:
          return 'r';

class connection:
  def __init__(self, p, parent, dist):
    self.p = p
    self.parent = parent
    self.dist = dist
  def __str__(self):
    return "(" + self.p + ", " + self.parent + ", " + str(self.dist) + ")"

class coordinate:
  def __init__(self, name, x, y, heuristica, orientation, esquina):
    self.name = name
    self.x = x
    self.y = y
    self.heuristica = heuristica
    self.orientation = orientation
    self.esquina = esquina
  def __str__(self):
    return "(" + self.name + ", x = " + str(self.x) + ", y = " \
	       + str(self.y) + ', hcost = ' + str(self.heuristica) + ', ori =' + self.orientation \
	       + ', corner = ' + str(self.esquina) +  ")"

class MyRob(CRobLink):
    #marca coordenadas
    def markCoordenate(self, parent, esquina):
      name = 'c' + str(self.coordNameCounter)
      if parent == None:
        heuristica = 0
      else:
        heuristica = dist((self.prevState.currX, self.prevState.currY), (self.coords[0].x, self.coords[0].y))

      mark = True

      if mark:
        orientation = self.get_orientation(self.measures)

        #acerto nas linhas (we are taking some risks here, but...)
        if parent != None:
          if orientation == 'e' or orientation == 'w':
            if parent.orientation == orientation:
              self.prevState.lastY = parent.y
              self.prevState.currY = parent.y
          elif orientation == 'n' or orientation == 's':
            if parent.orientation == orientation:
              self.prevState.lastX = parent.x
              self.prevState.currX = parent.x

        c = coordinate(name, self.prevState.currX, self.prevState.currY, heuristica, orientation, esquina)
        self.coordNameCounter += 1
        self.coords += [c]

        if parent != None:
          conn = connection(str(name), str(parent.name), \
	  dist((self.prevState.currX, self.prevState.currY), (parent.x, parent.y) ))

          self.conns += [conn]
    

    #marca pontos
    def mark(self):
        parent = self.getParent()

        #deteta que fez um ciclo possivelmente a volta do mapa
        if not self.loop and parent != None and dist((self.prevState.currX, self.prevState.currY), (self.coords[0].x, self.coords[0].y)) < 5 \
		and (self.prevState.maxX - self.prevState.minX) > self.mapWidth / 2 \
		and (self.prevState.maxY - self.prevState.minY) > self.mapHeight / 2:
          #determina se vai para o meio da area do ciclo que foi percorrido
          maxX = (self.prevState.maxX - self.prevState.minX)
          maxY = (self.prevState.maxY - self.prevState.minY)
          self.mapCenter = (self.prevState.minX + maxX / 2, self.prevState.minY + maxY / 2)
          nearCenterVisited = [n for n in self.coords if dist((n.x, n.y), self.mapCenter ) < 5]
          if nearCenterVisited == []:
            self.path = [self.mapCenter]
            self.loop = True
            self.prev_state = ''
            self.state = 'run'
        elif self.loop:
          #esta perto do centro
          if dist((self.prevState.currX, self.prevState.currY), self.mapCenter) < 2:
            self.loop = False

        #marca o ponto
        if parent == None or abs(parent.x - self.prevState.currX) > 0.5 or abs(parent.y - self.prevState.currY) > 0.5:
          self.markCoordenate(parent, False)

    #determina a potencia a dar aos motores
    def drive(self, measures, state, prev_state, fase, beaconBlocked, prevState):
      adjust = ''

      (left, center, right, back, compass, collision, beaconVisible, beaconDir) = self.getDataFromSensors(measures)
      orientation = self.get_orientation(measures)

      #print fase
      """
      read(measures)

      a = self.emergencyprev_state(sensors, state, prev_state)
      if a != None:
        return a
      """
      #desbloqueia o farol caso o agente o tenha bloqueado para contornar o obstaculo
      if beaconBlocked and not beaconVisible:
        beaconBlocked = False

      if not self.targetVisible and self.targetBlocked:
        self.targetBlocked = False

      """
      if collision:
        prev_state = state
        state = 'deadend'
        if fase == 2:
            if self.targetVisible:
              self.targetBlocked = True

            return (state, prev_state, adjust, beaconBlocked, beaconDir, orientation)
      """
      if left >= 1.5 and right >= 1.5 and center < 1.5 and state != 'passage' and state != 'deadend' \
		     and state != 'rotateR' and state != 'rotateL':
          prev_state = state
          state = 'passage'

          #last_safe_position = (measures.x, measures.y)
          #marcar safe location aqui
      elif left >= 1.5 and right >= 1.5 and center >= 1.5 and state != 'deadend' and state != 'followWL' \
		       and state != 'rotateR' and state != 'rotateL' and state != 'followWR':
          prev_state = state
          state = 'deadend'
          #self.backwards = True
          #marcar obstaculo
      #viu o farol mas pode ter um obstaculo a frente por isso tem de o ignorar
      elif fase == 1 and beaconVisible and not beaconBlocked:
        #print 'every day more and more'
        self.path = []
        prevState.seenBeacon = True
        #nao tem nada a frente
        if center < 1.5:
          if state != 'runToBeacon':
            prev_state = state
            state = 'runToBeacon'
          #print "run to beacon"
        #obstaculo ignora o beacon
        else:
          if prev_state != 'passage':
            state = prev_state

          prev_state = 'runToBeacon'
          beaconBlocked = True
      
      #clean up
      if state != 'followWR':
        prevState.numTurnsR = 0
        prevState.turnR = False
      if state != 'followWL':
        prevState.numTurnsL = 0
        prevState.turnL = False

      if fase == 1 and self.exploringStrategy == 0:
        if dist((self.coords[0].x, self.coords[0].y), (self.prevState.currX, self.prevState.currY)) < 2 \
		and orientation != self.coords[0].orientation:
          self.exploringStrategy = 1

      #im probably lost
      if fase == 2 and state == 'followWR':
        targetXPos= self.path[0][0]
        if center > 1.5 and self.prevState.currX < targetXPos \
			and self.prevState.currY + 1 > self.path[0][1] \
			and (orientation == "n" or orientation == "ne" or orientation == "nw") \
			and [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x <= self.prevState.currX and n.name not in self.states] == []:
          print '-------------1'
          prev_state = 'followWL'
          state = 'rotateR'
          """
          if not self.home:
            vizinhos = [n for n in self.coords \
		        if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.name not in self.states]
            if
            vizinhos.sort(key = lambda n: n.heuristica)
            self.target = vizinhos[0]
            self.states = [self.target.name]         
            if self.target.name == 'c0':
              self.home = True
                 
            print self.target
            self.path = [(self.target.x, self.target.y)]
          """
        elif self.prevState.currX < targetXPos \
			and (orientation == "s" or orientation == "sw" or orientation == "se") \
			and [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x > self.prevState.currX - 1 and n.name not in self.states] == []:
          print '-------------20'
          prev_state = 'followWL'
          state = 'rotateR'
        #(problema das esquinas n.x < self.prevState.currX + 1.5)
        elif self.prevState.currX < targetXPos \
			and (orientation == "w" or orientation == "nw" or orientation == "sw") \
			and [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x < self.prevState.currX + 1.5 and n.y < self.prevState.currY + 1 and n.name not in self.states] == []:
          print '-------------2'
          prev_state = 'followWL'
          state = 'rotateR'
      elif fase == 2 and state == 'followWL':
        targetXPos= self.path[0][0]
        if center > 1.5 and self.prevState.currX > targetXPos \
			and self.prevState.currY + 1 > self.path[0][1] \
			and (orientation == "n" or orientation == "ne" or orientation == "nw") \
			and [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x >= self.prevState.currX and n.name not in self.states] == []:
          print '-------------3'
          prev_state = 'followWR'
          state = 'rotateL'
        #(problema das esquinas n.x < self.prevState.currX - 1.5)
        elif self.prevState.currX > targetXPos \
			and (orientation == "e" or orientation == "ne" or orientation == "se") \
			and [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x > self.prevState.currX - 1.5 and n.y < self.prevState.currY + 1 \
			and n.name not in self.states] == []:
          print '-------------4'
          prev_state = 'followWR'
          state = 'rotateL'

      if state == 'run':
        #return
        if fase == 2:
          if center > 1.5:# or left >= 1.5 or right >= 1.5:
            if self.targetVisible:
              self.targetBlocked = True
          """
          if left >= 2.5 or right >= 2.5:
            self.backwards = True
            print 'dead'
            #prev_state = state
            #state = 'deadend'
          """
          if center > 1.5:# or left >= 1.5 or right >= 1.5:
            #print 'c', orientation, self.targetBlocked
            if orientation == "n" or orientation == "ne" or orientation == "nw":
              print '000000', center

              targetXPos = self.path[0][0]
              #cornerMaxXPos = None

              neighbours = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.y < self.prevState.currY + 1 \
                        and n.y > self.prevState.currY - 1]
            
              if neighbours != [] and dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1])) > 2.5:
                neighbours.sort(key = lambda n: n.x)
                targetXPos = neighbours[0].x
                print 'n'
                #corner max x position
                for n in neighbours:
                  #print n
                  #target
                  if n.x == self.path[0][0] and n.y == self.path[0][1]:
                    targetXPos = n.x
                    break
                  if n.esquina:
                    targetXPos = n.x
                    print 'e'
                    #cornerMaxXPos = n.x

              if left < 1:
                print 'lft'
                prev_state = 'followWR'
                state = 'rotateL'             
              if targetXPos > self.prevState.currX:
                #test purpose only. Use comment
                prev_state = 'followWL'
                state = 'rotateR'
              else:
                prev_state = 'followWR'
                state = 'rotateL'

            #south
            elif orientation == "s" or orientation == "se" or orientation == "sw":
              print '111111'
              #find knowned map area

              targetXPos= self.path[0][0]

              vizinhos = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.y > self.prevState.currY - 1 and n.y < self.prevState.currY + 0.5 and n.name not in self.states]

              """
              vizinhos = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.y <= self.prevState.currY and n.name not in self.states]
              """
              if vizinhos == []:
                print 'vizinhos'

              if vizinhos != []:
                vizinhos.sort(key = lambda n: n.x)
                targetXPos = vizinhos[0].x

              #mesma linha que o target
              if self.prevState.currY < self.path[0][1] + 0.5 and self.prevState.currY > self.path[0][1] - 0.5:
                if self.path[0][0] > self.prevState.currX:
                  print '22'
                  prev_state = 'followWR'
                  state = 'rotateL'
                else:
                  print '33'
                  prev_state = 'followWL'
                  state = 'rotateR'

              #elif vizinhos == [] and self.home: #targetXPos == self.coords[0].x and self.path[0][1] == self.coords[0].y:
              #  print '1'

              elif targetXPos > self.prevState.currX:
                print '2'
                prev_state = 'followWR'
                state = 'rotateL'
              else:
                print '3'
                prev_state = 'followWL'
                state = 'rotateR'

            ##################################
            elif orientation == "e":# or orientation == "se" or orientation == "ne":
              print '33333'
              targetYPos= self.path[0][1]
              vizinhos = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x >= self.prevState.currX and n.y != self.prevState.currY and n.name not in self.states]

              esquinas = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x >= self.prevState.currX and n.y != self.prevState.currY and n.name not in self.states \
			and n.esquina]

              if esquinas != []:
                print 'ee'
                esquinas.sort(key = lambda n: n.y)
                targetYPos = esquinas[0].y

              elif vizinhos != []:
                vizinhos.sort(key = lambda n: n.y)
                targetYPos = vizinhos[-1].y

              if targetYPos > self.prevState.currY:
                prev_state = 'followWR'
                state = 'rotateL'
              else:
                prev_state = 'followWL'
                state = 'rotateR'
            elif orientation == "w":# or orientation == "sw" or orientation == "nw":
              print '4444'
              targetYPos= self.path[0][1]

              #conheco a parte de cima
              rSide = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 3 \
                        and n.x > self.prevState.currX - 1 and n.y > self.prevState.currY and n.name not in self.states]

              lSide = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 3 \
                        and n.x > self.prevState.currX - 1 and n.y < self.prevState.currY and n.name not in self.states]
              
              vizinhos = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                        and n.x <= self.prevState.currX and n.y != self.prevState.currY and n.name not in self.states]
   
              if vizinhos != []:
                vizinhos.sort(key = lambda n: n.y)
                targetYPos = vizinhos[0].y
                print 'uv', vizinhos[0], self.prevState.currX, self.prevState.currY
           
              if rSide != []:
                print 'u1'
                prev_state = 'followWL'
                state = 'rotateR'
              elif lSide != []:
                print 'u2'
                prev_state = 'followWR'
                state = 'rotateL'            
              elif targetYPos > self.prevState.currY:
                prev_state = 'followWL'
                state = 'rotateR'
              else:
                prev_state = 'followWR'
                state = 'rotateL'
          ####

          elif left > 2:
            prev_state = state
            state = 'followWL'
            adjust = 'r'
          elif right > 2:
            prev_state = state
            state = 'followWR'
            adjust = 'l'
          elif left >= 1.5:
              print 'lft', orientation
              prev_state = state
              state = 'followWL'   
          elif right >= 1.5:
              print 'rgh', orientation
              prev_state = state
              state = 'followWR'

        #exploratory phase
        else:
          if not prevState.seenBeacon and prev_state == 'followWR' and center > 1.5:
            #print 'choose'
            side = prevState.chooseSide(orientation, self.prevState.currX, self.prevState.currY)
            if side == 'l':
              prev_state = state
              state = 'followWL'    
            else:  
              prev_state = state
              state = 'followWR'      
          #estava a seguir a direita e viu o beacon e encontrou obstaculo a frente segue a direita
          elif prevState.seenBeacon and prev_state == 'followWR' and center > 1.5:
            prev_state = state
            state = 'followWR'  
          #estava a seguir a esquerda e viu o beacon e encontrou obstaculo a frente segue a esquerda
          elif prevState.seenBeacon and prev_state == 'followWL' and center > 1.5:
            prev_state = state
            state = 'followWL'    
          #tenho de ver se estou a seguir para um no (usar a orientacao para decidir que lado seguir)
          elif left >= 1.5 and prev_state != 'followWR':
            prev_state = state
            state = 'followWL'
          #substituir por else ?
          elif right >= 1.5:
            prev_state = state
            state = 'followWR'
          elif center > 1.5:
            prev_state = state
            state = 'followWR'      
      #seguir parede a esquerda
      elif state == 'followWL':
        #print center, left
        #parede a frente
        if center >= 3 or left >= 3:
          prev_state = state
          state = 'deadend'
          #self.backwards = True
        elif center > 1.5:
            prev_state = state
            state = 'rotateR'
            prevState.numTurnsL = 0
        #esta-se a aproximar da parede
        elif left > 2.5:
          adjust = 'r'
       #fim da parede
        elif left < 1.0:
          adjust = 'turnL'
          prevState.turnL = True
          if fase == 1:
            parent = self.getParent()
            self.markCoordenate(parent, True)
        #esta-se a afastar da parede
        elif left < 1.5:
          adjust = 'l'
          #se viu o farol muda de estrategia para seguir parede
          if fase == 1 and not prevState.seenBeacon and self.exploringStrategy == 0:
            if prevState.turnL:
              prevState.numTurnsL += 1
              prevState.turnL = False
            if prevState.numTurnsL == 2:
              prevState.numTurnsL = 0
              prev_state = state
              state = 'run'  
          else:
            prevState.turnL = False
            prevState.numTurnsL = 0

      #seguir parede a direita
      elif state == 'followWR':
        #print center, right
        #parede a frente
        #print center, right, state
        if center >= 3 or right >= 3:
          print 'ooooooooooo'
          prev_state = state
          state = 'deadend'
          #self.backwards = True
        elif center > 1.5:
            print 'ppp'
            prev_state = state
            state = 'rotateL'
            prevState.numTurnsR = 0
        #esta-se a aproximar da parede
        elif right > 2.5:
          adjust = 'l'
        #fim da parede
        elif right < 1.0:
          if fase == 1:
            parent = self.getParent()
            self.markCoordenate(parent, True)
          prevState.turnR = True
          adjust = 'turnR'
        #esta-se a afastar da parede
        elif right < 1.5:
          adjust = 'r'
          #se viu o farol muda de estrategia para seguir parede
          if fase == 1 and not prevState.seenBeacon and self.exploringStrategy == 0:
            if prevState.turnR:
              prevState.numTurnsR += 1
              prevState.turnR = False
            if prevState.numTurnsR == 2:
              prevState.numTurnsR = 0
              prev_state = state
              state = 'run' 
          else:
            prevState.turnR = False
            prevState.numTurnsR = 0       
      #rodar para a esquerda
      elif state == 'rotateL':
        #if center <= 0.5 and right < 1.5:
         if center <= 0.5:
          state = prev_state       
          prev_state = 'rotateL'
      #rodar para a direita
      elif state == 'rotateR':
        #if center <= 0.5 and left < 1.5:
        if center <= 0.5:
          state = prev_state       
          prev_state = 'rotateR'
      elif state == 'deadend':
          """
          if center > 1.5:
            pass
          elif center <= 1.5:
            if prev_state == 'followWR' and right <= 2.5:
              print 'o'
              prev_state = 'followWR'       
              state = 'rotateL'       
            elif prev_state == 'followWL' and left <= 2.5:
              print 'oo'
              prev_state = 'followWL'       
              state = 'rotateR'
            elif prev_state == 'passage':
              if left < 1:
                prev_state = 'followWR'       
                state = 'rotateL'
              elif right < 1:
                prev_state = 'followWL'       
                state = 'rotateR'  
          """

          print 'probbbbbbb', prev_state
          """
          if prev_state == 'followWR' and right <= 2:
            print 'prb0'
            prev_state = 'followWR'       
            state = 'rotateL'
          elif prev_state == 'followWL' and left <= 2:
            print 'prb00'
            prev_state = 'followWL'       
            state = 'rotateR'  
          """
          if center < 1.5 and left < 1:
            print 'prb1'
            prev_state = 'followWR'       
            state = 'rotateL'
          elif center < 1.5 and right < 1:
            print 'prb2'
            prev_state = 'followWL'       
            state = 'rotateR'  

      #elif state == 'return':
      #  pass
      #vai para o farol
      elif state == 'runToBeacon' or state == 'runToCenter':
        #tem obstaculo a direita afasta-se
        if right > 2.5:
          adjust = 'l'
        #tem obstaculo a esquerda afasta-se
        elif left > 2.5:
          adjust = 'r'
      #esta a passar num corredor (tenho de ver se consigo coloca-lo no meio da passagem)
      elif state == 'passage':
        if prev_state == 'followWL':
          """
          if left > 3.5:
            adjust = 'r'
          elif left < 3.0:
            adjust = 'l'
          """
          if right < 1.0:
            prev_state = state
            state = 'followWL'
          elif center >= 1.5:
            prev_state = state
            state = 'deadend'
            #self.backwards = True
        elif prev_state == 'followWR':
          #deixa-o passar no ultimo mapa na passagem
          if right > 2.5:
            adjust = 'l'
          elif right < 1.5:
            adjust = 'r'
          elif left < 1.0:
            prev_state = state
            state = 'followWR'
          elif center >= 1.5:
            prev_state = state
            state = 'deadend'
            #self.backwards = True

      #print state, prev_state, adjust
      return (state, prev_state, adjust, beaconBlocked, beaconDir, orientation)

    def markInGrid(self):
      #refactor this
      orientation = self.get_orientation(self.measures)
      (left, center, right, back, compass, collision, beaconVisible, beaconDir) = self.getDataFromSensors(self.measures)

      currCell = self.g.getCurrent((self.prevState.currX, self.prevState.currY))
      if currCell == None:
        return None

      currCell.obstacle = False
      currCell.visitado = True
      

      if orientation == 'e':
          lCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y + 1)
          rCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y - 1)
          fCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y)
      elif orientation == 'ne':
          pass
      elif orientation == 'se':
          pass
      elif orientation == 'w':
          lCell = self.g.getCellByXAndY(currCell.x - 1, currCell.y - 1)
          rCell = self.g.getCellByXAndY(currCell.x - 1, currCell.y + 1)
          fCell = self.g.getCellByXAndY(currCell.x - 1, currCell.y)
      elif orientation == 'nw':
          pass
      elif orientation == 'sw':
          pass
      elif orientation == 'n':
          lCell = self.g.getCellByXAndY(currCell.x - 1, currCell.y + 1)
          rCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y + 1)
          fCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y + 1)
      elif orientation == 's':
          lCell = self.g.getCellByXAndY(currCell.x + 1, currCell.y - 1)
          rCell = self.g.getCellByXAndY(currCell.x - 1, currCell.y - 1)
          fCell = self.g.getCellByXAndY(currCell.x, currCell.y - 1)

      if orientation == 's' or orientation == 'n' or orientation == 'e' or orientation == 'w':
          #print 'l', left
          if lCell != None:
            if left <= 1.5 and not lCell.visitado:
              lCell.candidate = True
            elif left > 1.5:
              lCell.obstacle = True

          if rCell != None:
            if right <= 1.5 and not rCell.visitado:
              rCell.candidate = True
            elif right > 1.5:
              rCell.obstacle = True

          if fCell != None:
            if center <= 1.5 and not fCell.visitado:
              fCell.candidate = True
            elif center > 1.5 and not fCell.visitado:
              fCell.obstacle = True

      return currCell
    #run
    def run(self):
        if rob.status!=0:
            print "Connection refused or error"
            quit()

        self.exploringStrategy = 0
        self.coords = []
        self.conns = []
        self.markPos = 25
        #usado para dar um nome a coordenada ex: c1, c2, ..., cn
        self.coordNameCounter = 0
        #contador para marcar os pontos
        self.countCycles = 0
        self.loop = False
        self.mapCenter = None
        self.path = []

        #constants
        self.mapWidth = 28
        self.mapHeight = 14
        self.robotDiameter = 1
        self.obstMinWidth = 0.3
        self.passagetMinWidth = 1.5
        self.gpsDeviation = 0.5

        self.turnToTarget = False

        #infor sobre o estado
        self.state = 'stop'
        self.prev_state = ''
        #self.stoppedState = 'run'
        self.fase = 0

        #ultima posicao segura conhecida (evitar loops e dead ends)
        self.last_safe_position = None

        self.start_saved = False
        self.prev_ground = 99
        self.counter = 0
        self.beaconBlocked = False
      
        self.prevState = prevAgentState()
        #self.points = []
        self.pointCounter = 1
        self.step = 10
        self.followDistance = 0
        self.targetVisible = False
        self.targetBlocked = False
        self.states = []
        self.home = False  
        self.backwards = False
        #self.tamanho = 0.5
        self.lastOrientation = None
        self.myStrategy = 1
        self.lastVisitedCell = []

        while True:
            self.readSensors()
            if self.measures.start:
              orientation = self.get_orientation(self.measures)
              x = self.measures.x
              y = self.measures.y
              #experience
              if self.lastOrientation == None:
                self.lastOrientation = orientation
              else:
                if orientation == 'e' or orientation == 'w' and self.lastOrientation == orientation \
			and (y > self.prevState.lastY + 0.25 or y < self.prevState.lastY - 0.25):
                  y = self.prevState.lastY
                elif orientation == 'n' or orientation == 's' and self.lastOrientation == orientation \
			and (x > self.prevState.lastX + 0.25 or x < self.prevState.lastX - 0.25):
                  x = self.prevState.lastX
                else:
                  self.lastOrientation = orientation
              
              self.prevState.setCurrentPosition(x, y)
              #self.prevState.setCurrentPosition(self.measures.x, self.measures.y)


            if (self.measures.groundReady):
                if self.measures.ground != self.prev_ground:
                    print self.state, "ground=", self.measures.ground
                    self.prev_ground = self.measures.ground
 
            if self.measures.endLed:
                print self.robName + " exiting"
                quit()

            if (self.state == 'stop' and self.measures.start):
                self.state = 'run'
                self.fase = 1

            if (self.state != 'stop' and self.measures.stop):
                self.state = 'stop'

            if (self.fase == 1):
                if not self.start_saved:
                    if self.measures.gpsReady:
                        self.prevState.setCurrentPosition(self.measures.x, self.measures.y)
                        #p = point('inicio', self.prevState.currX, self.prevState.currY, 'v', ' ')
			#self.points += [p]
                        #jose
                        self.mark()
                        #self.coordNameCounter += 1

                        self.prevState.beginPos = ('inicio', self.prevState.currX, self.prevState.currY)
                        #self.start_pos = (self.prevState.currX, self.prevState.currY)

                        self.g = Grelha2((self.prevState.currX, self.prevState.currY), 0.5)

                        self.start_saved = True
                if not self.measures.visitingLed and \
                   not self.measures.returningLed and \
                   self.measures.groundReady and self.measures.ground==0:
                    self.setVisitingLed(True)
                    print self.robName + " visited target area"
                if self.measures.visitingLed:
                    self.setVisitingLed(False)
                    self.setReturningLed(True)
                if self.measures.returningLed:
                    self.prev_state = self.state
                    self.fase = 2
                    self.state = 'return'
                    self.prev_state = ''
                    self.state = 'run'
                    self.prevState.seenBeacon = False

                    
                    self.lastVisitedCell.end = 1
                    #self.g.printMe()

                    temp = []
                    for u in self.g.all_nodes:
                      if u.visitado or u.candidate or u.obstacle:
                        temp += [u]

                    self.g.all_nodes = temp
                    """
                    self.bestPath = SearchDomain(self.g)
                    prob = SearchProblem(self.bestPath, self.lastVisitedCell)
                    my_tree = SearchTree(prob)
                    path = my_tree.search()
                    
                    r = ''
                    l = ''
                    for t in path:
                      if (t.x != 0 or t.y != 0) and (self.lastVisitedCell.x != t.x or self.lastVisitedCell.y != t.y \
				   and self.lastVisitedCell.end == 1):
                        c = self.g.getCellByXAndY(t.x, t.y)
                        c.path = 1

                      r += '(' + str(t.x) + ',' + str(t.y) + ')'
                    print r
                    print len(path)
                    self.g.printMe()
                    
                    quit()
                    """


                    vizinhos = [n for n in self.coords \
			if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5]

                    vizinhos.sort(key = lambda n: n.heuristica)
                    self.target = vizinhos[0]
                    #self.coordsParent = [n for n in self.coords if n.name == self.conns[-1].parent][0]

                    #print 'pi', self.target
                    self.states = [self.target.name]
                    """
                    target = self.conns[-2].parent
                    #print target
                    self.conns = self.conns[0: -2]
                    """
                    #targetCoords = [n for n in self.coords if n.name == target]
                    #self.path = [(targetCoords[0].x, targetCoords[0].y)]

                    if self.myStrategy == 1:
                      n = self.g.bestDecision(self.lastVisitedCell.gps, 5)
                      if n == None:
                        self.path = [(self.target.x, self.target.y)]
                      else:
                        self.path = [(n.gps[0], n.gps[1])]
                      print 'celll', n, self.path
                    else:
                      self.path = [(self.target.x, self.target.y)]
                    #self.followDistance = dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1]))
                else:
                    (lPow,rPow) = self.determineAction()
                    self.driveMotors(lPow,rPow)

            if (self.fase == 2):
                #not working
                #if self.measures.groundReady and self.measures.ground==1:
                #    self.finish()
                if self.measures.gpsReady and \
                    dist((self.prevState.currX, self.prevState.currY), (self.coords[0].x, self.coords[0].y)) < 0.5:
                    self.finish()
                #se vi o fim vou para la logo
                elif not self.home and self.measures.gpsReady \
			and dist((self.prevState.currX, self.prevState.currY), (self.coords[0].x, self.coords[0].y)) <= 5:
                    print 'home'
                    self.prev_state = ''
                    self.state = 'run'
                    self.path = [(self.coords[0].x, self.coords[0].y)]
                    self.home = True
                    self.target = self.coords[0]
                    self.states = ['c0'] 
                elif self.measures.gpsReady and \
                    dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1])) < 0.5:
                    #dist((self.measures.x,self.measures.y),self.start_pos)<0.5:
                    #if self.path == []:
                    if self.path[0][0] == self.coords[0].x and self.path[0][1] == self.coords[0].y:
                        #print  self.path[0][0], self.coords[0].x, self.path[0][1], self.coords[0].y
                        self.finish()
                    #escolhe o proximo alvo
                    else:
                          self.changeTarget()
                          """
                          self.state = 'run'

                          #parent = [n.parent for n in self.conns if n.p == self.target.name][0]
                          #self.coordsParent = [n for n in self.coords if n.name == parent][0]

                          vizinhos = [n for n in self.coords \
			              if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                                      and n.name not in self.states]

                          vizinhos.sort(key = lambda n: n.heuristica)
                          self.target = vizinhos[0]
                          self.states = [self.target.name]         
                          if self.target.name == 'c0':
                            self.home = True
                 
                          #print 'py', self.target
                          #self.path = [(self.target.x, self.target.y)]
                          if self.myStrategy == 1 and not self.home:
                            c = self.g.getCurrent((self.path[0][0], self.path[0][1]))
                            if c != None:
                              n = self.g.bestDecision(c.gps, 5)

                            if n == None and vizinhos != []:
                              self.path = [(self.target.x, self.target.y)]
                            else:
                              self.path = [(n.gps[0], n.gps[1])]
                            print 'celllpy', n, self.path
                          elif vizinhos != []:
                            self.path = [(self.target.x, self.target.y)]
                          """
                #bug do home
                elif self.home and \
                    dist((self.prevState.currX, self.prevState.currY), (self.coords[0].x, self.coords[0].y)) < 1.5:
                          self.prev_state = ''
                          self.state = 'run'

                elif self.measures.gpsReady and self.prevState.currY < self.path[0][1] + 0.5 \
				and self.prevState.currY > self.path[0][1] - 0.5 and \
				(orientation == 'ne' or orientation == 'se' or orientation == 'nw' or orientation == 'sw') \
                                and (self.home or self.state == 'deadend'):

                          self.prev_state = self.state
                          self.state = 'run'
                          print 'new'
                #try to optimize da erro no 2007
                
                elif self.measures.gpsReady and self.state == 'run' and not self.home and \
                    dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1])) < 2.5:
                          self.changeTarget()
                          """
                          self.prev_state = ''
                          self.state = 'run'

                          vizinhos = [n for n in self.coords \
			              if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                                      and n.name not in self.states]

                          if vizinhos != []:
                            vizinhos.sort(key = lambda n: n.heuristica)
                            self.target = vizinhos[0]
                            self.states = [self.target.name]         
                            print self.states
                            if self.target.name == 'c0':
                              self.home = True
                 
                            #print 'po', self.target
                            #self.path = [(self.target.x, self.target.y)]
                          if self.myStrategy == 1 and not self.home:
                            c = self.g.getCurrent((self.path[0][0], self.path[0][1]))
                            if c != None:
                              n = self.g.bestDecision(c.gps, 5)

                            if n == None and vizinhos != []:
                              self.path = [(self.target.x, self.target.y)]
                            else:
                              self.path = [(n.gps[0], n.gps[1])]
                            print 'celllpt', n, self.path
                          elif vizinhos != []:
                            self.path = [(self.target.x, self.target.y)]
                          """
                
                (lPow,rPow) = self.determineAction()
                self.driveMotors(lPow,rPow)

    def changeTarget(self):
      self.prev_state = ''
      self.state = 'run'

      vizinhos = [n for n in self.coords \
	          if dist((self.prevState.currX, self.prevState.currY), (n.x, n.y)) < 5 \
                  and n.name not in self.states]

      if vizinhos != []:
        vizinhos.sort(key = lambda n: n.heuristica)
        self.target = vizinhos[0]
        self.states = [self.target.name]         
        #print self.states
        if self.target.name == 'c0':
          self.home = True
                 
      if self.myStrategy == 1 and not self.home:
        n = None
        #c = self.g.getCurrent((self.path[0][0], self.path[0][1]))
        c = self.g.getCurrent((self.prevState.currX, self.prevState.currY))
        if c != None:
          n = self.g.bestDecision(c.gps, 5)

        if n == None:
          if vizinhos != []:
            self.path = [(self.target.x, self.target.y)]
        else:
          self.path = [(n.gps[0], n.gps[1])]
        print 'celllpt', n, self.path
      elif vizinhos != []:
          self.path = [(self.target.x, self.target.y)]

    def determineAction(self):
      #print self.prevState.currX, self.prevState.currY
      #print self.state
      adjust = ''

      #mark points creating a path
      
      if self.countCycles == self.markPos and self.fase == 1:
        if self.measures.gpsReady:
            self.mark()
            #self.coordNameCounter += 1
        self.countCycles = 0
      
      #print self.prevState.currX, self.prevState.currY
      if self.fase == 1:
        temp = self.markInGrid()
        if temp != None:
          self.lastVisitedCell = temp

      lPow = 0.0
      rPow = 0.0

      #clears objective in fase 1 (this could be the calculated center of the map)
      if self.fase == 1 and self.path != [] and dist((self.prevState.currX, self.prevState.currY), self.path[0]) < 0.5:
        self.path = []

      #no target to follow
      if self.path == []:
         (self.state, self.prev_state, adjust, self.beaconBlocked, beaconDir, orientation) = \
						self.drive(self.measures, \
						self.state, self.prev_state, self.fase, self.beaconBlocked, \
						self.prevState)
      #follow target
      else:
        self.blockUnblockTarget()
        target_dir = self.currDiretionToTarget()

        #print self.state, self.mapCenter
        if not self.targetBlocked and self.state == 'run':
          self.turnToTarget = True

          if target_dir > 15.0:
            lPow=-0.05
            rPow=0.05
          elif target_dir < -15.0:
            lPow=0.05
            rPow=-0.05
          else:
            self.turnToTarget = False
            (self.state, self.prev_state, adjust, self.beaconBlocked, beaconDir, orientation) = \
						self.drive(self.measures, \
						self.state, self.prev_state, self.fase, self.beaconBlocked, \
						self.prevState)
        else:
          if (self.state == 'followWR' or self.state == 'followWL') and (target_dir < 15.0 and target_dir > -15.0):
          #if (self.state != 'turnR' and self.state != 'turnL' and self.state != 'deadend') and (target_dir < 15.0 and target_dir > -15.0):
            self.prev_state = ''
            self.state = 'run'    
            #print 'look'        
          #else:
          (self.state, self.prev_state, adjust, self.beaconBlocked, beaconDir, orientation) = \
						self.drive(self.measures, \
						self.state, self.prev_state, self.fase, self.beaconBlocked, \
						self.prevState)
        
      #determina os maximos e minimos percorridos (tentar saber o tamanho do mapa)
      self.recordMinMaxDistances()

      #power engine according to state
      if (self.measures.collisionReady):
        collision = self.measures.collision
          
      #print self.state, adjust, collision

      if self.backwards:
        lPow = -0.08
        rPow = -0.08
        self.backwards = False
        if self.fase == 2:
          #self.prev_state = self.state
          #self.state = 'deadend'
          if self.targetVisible:
            self.targetBlocked = True
      elif collision:
        
        if self.fase == 1:
          self.prev_state = self.state
          self.state = 'deadend'
        
        elif self.fase == 2:
          #self.prev_state = self.state
          #self.state = 'deadend'
          if self.targetVisible:
            self.targetBlocked = True

        lPow = -0.04
        rPow = -0.04
      elif self.state == 'run' and not self.turnToTarget:
        lPow = 0.1
        rPow = 0.1
      elif self.state == 'followWL':
        if adjust == 'r':
          lPow = 0.04
          rPow = 0.0
        elif adjust == 'turnL':
          lPow = 0.03
          rPow = 0.1
        elif adjust == 'l':
          lPow = 0.0
          rPow = 0.04
        else:
          lPow = 0.1
          rPow = 0.1
      elif self.state == 'followWR':
        if adjust == 'r':
          lPow = 0.04
          rPow = 0.0
        elif adjust == 'turnR':
          lPow = 0.1
          rPow = 0.03
        elif adjust == 'l':
          lPow = 0.0
          rPow = 0.04
        else:
          lPow = 0.1
          rPow = 0.1
      elif self.state == 'rotateL':
          lPow = -0.03
          rPow = 0.03
      elif self.state == 'rotateR':
          lPow = 0.03
          rPow = -0.03
      elif self.state == 'deadend':
          lPow = -0.04
          rPow = -0.04
      elif self.state == 'return':
        pass
      elif self.state == 'runToBeacon':
        if adjust == 'r':
          lPow = 0.04
          rPow = 0.0
        elif adjust == 'l':
          lPow = 0.0
          rPow = 0.04
        elif beaconDir > 15.0:
          lPow=0.0
          rPow=0.05
        elif beaconDir < -15.0:
          lPow=0.05
          rPow=0.0
        else:
          lPow=0.1
          rPow=0.1
      elif self.state == 'passage':
        if adjust == 'r':
          lPow = 0.04
          rPow = 0.0
        elif adjust == 'l':
          lPow = 0.0
          rPow = 0.04
        else:
          lPow = 0.08
          rPow = 0.08

      self.counter += 1
      self.countCycles += 1

      if lPow == 0 and rPow == 0:
        print 'stoped'
      #print lPow, rPow
      return lPow, rPow

    #block target visibillity
    def blockUnblockTarget(self):
      #target is visible
      if not self.targetVisible and dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1])) < 5:
        self.targetVisible = True
      #
      elif dist((self.prevState.currX, self.prevState.currY), (self.path[0][0], self.path[0][1])) > 6.5:
        self.targetVisible = False
        self.targetBlocked = False

    #records min and max map width and height (used to try to find the map center)
    def recordMinMaxDistances(self):
      if self.prevState.maxX == -1:
        self.prevState.maxX = self.prevState.currX
        self.prevState.maxY = self.prevState.currY
        self.prevState.minX = self.prevState.currX
        self.prevState.minY = self.prevState.currY
      else:
        if self.prevState.currX > self.prevState.maxX:
          self.prevState.maxX = self.prevState.currX
        if self.prevState.currY > self.prevState.maxY:
          self.prevState.maxY = self.prevState.currY

        if self.prevState.currX < self.prevState.minX:
          self.prevState.minX = self.prevState.currX
        if self.prevState.currY < self.prevState.minY:
          self.prevState.minY = self.prevState.currY

    #get direction to target position
    def currDiretionToTarget(self):
      dx = self.path[0][0] - self.prevState.currX
      dy = self.path[0][1] - self.prevState.currY

      abs_target_dir = atan2(dy, dx) * 180 / 3.141592
      target_dir = abs_target_dir - self.measures.compass

      if target_dir > 180:
        target_dir -= 360
      elif target_dir < -180:
        target_dir += 360

      return target_dir

    #get parent node from recorded path
    def getParent(self):
        if self.coords != []:
          return self.coords[-1]

        return None

    #get agent orientation [8 directions]
    def get_orientation(self, sensor):
      #each direction is calculated in 45 graus
      if sensor.compassReady:
        if sensor.compass <= 22.5 and sensor.compass >= -22.5:
          return "e"
        elif sensor.compass <= 67.5 and sensor.compass > 22.5:
          return "ne"
        elif sensor.compass <= 112.5 and sensor.compass > 67.5:
          return "n"
        elif sensor.compass <= 157.5 and sensor.compass > 112.5:
          return "nw"
        elif sensor.compass <= -112.5 and sensor.compass > -157.5:
          return "sw"
        elif sensor.compass <= -67.5 and sensor.compass > -112.5:
          return "s"
        elif sensor.compass < -22.5 and sensor.compass > -67.5:
          return "se"

      return "w"

    #get sensors info
    def getDataFromSensors(self, measures):
      if measures.gpsReady and measures.compassReady:
        compass = measures.compass

      if measures.irSensorReady[1]:
        left = measures.irSensor[1]

      if measures.irSensorReady[2]:
        right = measures.irSensor[2]

      if measures.irSensorReady[0]:
        center = measures.irSensor[0]

      if measures.irSensorReady[3]:
        back = measures.irSensor[3]

      beaconReady = measures.beaconReady
      if(beaconReady):
        (beaconVisible, beaconDir) = measures.beacon

      if (measures.collisionReady):
        collision = measures.collision

      return (left, center, right, back, compass, collision, beaconVisible, beaconDir)

def dist(p,q):
    (px,py) = p
    (qx,qy) = q
    return sqrt((px-qx)**2 + (py-qy)**2)

rob=MyRob("AA",3,"localhost")
rob.run()
